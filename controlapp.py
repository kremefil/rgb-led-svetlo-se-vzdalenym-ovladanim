
from cmath import log10
import PyQt6
from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QGridLayout, QMainWindow, QComboBox, QLineEdit, QHBoxLayout, QVBoxLayout, QSlider, QPlainTextEdit, QGroupBox
from PyQt6.QtGui import QIcon, QFont
from PyQt6.QtCore import Qt, QTimer
import sys
import serial
import serial.tools.list_ports
import numpy as np
import time
import math
 
class DashBoard(QMainWindow):
    def __init__(self):
        super().__init__()
        #Dashboard settings
        self.setWindowTitle("RGB LED controller")
        self.setGeometry(750,350, 500,300)
        self.setWindowIcon(QIcon('smalldiscordlogoweb.png'))

        #Serial
        self.serial_port = serial.Serial()

        #Widgets
        self.btn_connect = QPushButton("Connect")
        self.btn_command = QPushButton("Send command")
        self.btn_color = QPushButton("Set color")
        self.btn_mode = QPushButton("Command mode")
        self.combo_baudrate = QComboBox()
        self.combo_port = QComboBox()
        self.label_baudrate = QLabel("Baudrate")
        self.label_port = QLabel("COM port")
        self.label_red = QLabel("PWM: ---% for RED LED")
        self.label_green = QLabel("PWM: ---% for GREEN LED")
        self.label_blue = QLabel("PWM: ---% for BLUE LED")
        self.textbox = QPlainTextEdit()
        self.textbox.setPlainText("App is running....")

        self.commandline = QLineEdit()
        
        self.slider_r = QSlider(Qt.Orientation.Horizontal)
        self.slider_g = QSlider(Qt.Orientation.Horizontal)
        self.slider_b = QSlider(Qt.Orientation.Horizontal)
        self.slider_r.setMaximum(250)
        self.slider_g.setMaximum(250)
        self.slider_b.setMaximum(250)

        
        #Layouts
        layout_main = QVBoxLayout()
        layout_labels = QVBoxLayout()
        layout_combos = QVBoxLayout()
        layout_textbox = QVBoxLayout()
        layout_horizontal = QHBoxLayout()
       
        # layout_labels.addWidget(self.label_baudrate)
        # layout_labels.addWidget(self.label_port)
        # layout_labels.addWidget(self.btn_connect)
        
        layout_combos.addWidget(self.label_baudrate)
        layout_combos.addWidget(self.combo_baudrate)
        layout_combos.addWidget(self.label_port)
        layout_combos.addWidget(self.combo_port)
        layout_combos.addWidget(self.btn_connect)
        layout_combos.addWidget(self.btn_color)
        

        layout_textbox.addWidget(self.textbox)
        layout_textbox.addWidget(self.commandline)
        layout_textbox.addWidget(self.btn_command)
        layout_textbox.addWidget(self.btn_mode)
        
        
       
        layout_horizontal.addLayout(layout_labels)
        layout_horizontal.addLayout(layout_combos)
        layout_horizontal.addLayout(layout_textbox)
        layout_main.addLayout(layout_horizontal)
        layout_main.addWidget(self.label_red)
        layout_main.addWidget(self.slider_r)
        layout_main.addWidget(self.label_green)
        layout_main.addWidget(self.slider_g)
        layout_main.addWidget(self.label_blue)
        layout_main.addWidget(self.slider_b)

        widget = QWidget()
        widget.setLayout(layout_main)
        self.setCentralWidget(widget)

        #Funcionality
        ports = serial.tools.list_ports.comports()
        for port in sorted(ports):
            self.combo_port.addItem(str(port)[:5])
        self.combo_baudrate.addItems(["9600","115200"])
        self.btn_connect.clicked.connect(self.connect_port)
        self.btn_command.clicked.connect(self.send_command)
        self.btn_mode.clicked.connect(self.command_mode)
        self.btn_color.clicked.connect(self.set_color)
        self.slider_r.valueChanged.connect(self.PWM_r)
        self.slider_g.valueChanged.connect(self.PWM_g)
        self.slider_b.valueChanged.connect(self.PWM_b)
        self.locker = 0
        self.previous_r = 0
        self.previous_g = 0
        self.previous_b = 0
       
        self.L =[ 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 
	    1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 
	    2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 
	    3, 3, 4, 4, 4, 4, 4, 4, 5, 5, 
	    5, 5, 5, 5, 6, 6, 6, 6, 6, 7, 
	    7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 
	    9, 10, 10, 10, 11, 11, 11, 12, 12, 12, 
	    13, 13, 13, 14, 14, 14, 15, 15, 16, 16, 
	    16, 17, 17, 18, 18, 19, 19, 19, 20, 20, 
	    21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 
	    26, 27, 27, 28, 28, 29, 30, 30, 31, 31, 
	    32, 33, 33, 34, 35, 35, 36, 37, 37, 38, 
	    39, 40, 40, 41, 42, 43, 43, 44, 45, 46, 
	    47, 47, 48, 49, 50, 51, 52, 53, 54, 54, 
	    55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 
	    65, 66, 67, 68, 69, 70, 71, 73, 74, 75, 
	    76, 77, 78, 79, 81, 82, 83, 84, 85, 87, 
	    88, 89, 90, 92, 93, 94, 96, 97, 98, 100, 
	    101, 102, 104, 105, 107, 108, 110, 111, 113, 114, 
	    116, 117, 119, 120, 122, 123, 125, 126, 128, 130, 
	    131, 133, 135, 136, 138, 140, 141, 143, 145, 147, 
	    148, 150, 152, 154, 156, 157, 159, 161, 163, 165, 
	    167, 169, 171, 173, 175, 177, 179, 181, 183, 185, 
	    187, 189, 191, 193, 195, 197, 200, 202, 204, 206, 
	    208, 211, 213, 215, 217, 220, 222, 224, 227, 229, 
	    232]

    def connect_port(self):
        if(self.serial_port.isOpen() == True):
            self.serial_port.close()
            self.textbox.setPlainText("Disonnected!")
            self.btn_connect.setText("Connect")
        else:
            try:
                self.serial_port.port = self.combo_port.currentText()
                self.serial_port.baudrate = int(self.combo_baudrate.currentText())
                self.serial_port.timeout = 0
                self.serial_port.open()
                self.textbox.setPlainText("Connected!")
                self.btn_connect.setText("Disconnect")
                                
            except:
                self.textbox.setPlainText("Cannot open PORT!")
        
    def send_command(self):
        if(self.serial_port.isOpen() == True):
            for i in self.commandline.text():
                    self.serial_port.write(i.encode())
                    time.sleep(0.0001)
            self.textbox.appendPlainText("Command: " + "\"" + self.commandline.text() + "\"")
        else:
            self.textbox.setPlainText("Can't send a command, PORT is not opened!")
      



    #str(self.L[self.slider_r.value()])
    def PWM_r(self):
        if(self.serial_port.isOpen() == True and self.locker == 0):
            for i in str(self.L[self.slider_r.value()])[::-1] + ":R":
                self.serial_port.write(i.encode())
                time.sleep(0.00001)
                #temp_string = str(round(self.slider_r.value()/250*100,2))
                self.label_red.setText("PWM: " + str(round(self.slider_r.value()/250*100,2)) +  " % - RED LED")  
        elif(self.serial_port.isOpen() == True and self.locker == 1):
            delta = self.slider_r.value() - self.previous_r
            if(self.slider_b.value() != 0):
                self.slider_b.setValue(self.slider_b.value() + delta)
            if(self.slider_g.value() != 0):
                self.slider_g.setValue(self.slider_g.value() + delta)
            self.label_red.setText("Color mode - R: " + str(round(self.slider_r.value()/250*100,2)) +  " %" + "G: " + str(round(self.slider_g.value()/250*100,2)) +  " %" + "B: " + str(round(self.slider_b.value()/250*100,2)) +  " %")
            self.label_green.setText("-----------------------")
            self.label_blue.setText("-----------------------")
            for i in str(self.L[self.slider_r.value()])[::-1] + "R" + str(self.L[self.slider_g.value()])[::-1] + "G" + str(self.L[self.slider_b.value()])[::-1] + "B":
                self.serial_port.write(i.encode())
                #self.textbox.appendPlainText(i)
                time.sleep(0.000000001)

            
        self.previous_r = self.slider_r.value()    

    def PWM_g(self):
        if(self.serial_port.isOpen() == True and self.locker == 0):
            for i in str(self.L[self.slider_g.value()])[::-1] + ":G":
                self.serial_port.write(i.encode())
                time.sleep(0.00001)
            #temp_string = str(round(self.slider_g.value()/250*100,2))
            self.label_green.setText("PWM: " + str(round(self.slider_g.value()/250*100,2)) +  " % - GREEN LED")

    def PWM_b(self):
        if(self.serial_port.isOpen() == True and self.locker == 0):
            for i in str(self.L[self.slider_b.value()])[::-1] + ":B":
                self.serial_port.write(i.encode())
                time.sleep(0.00001)
            #temp_string = str(round(self.slider_b.value()/250*100,2))
            self.label_blue.setText("PWM: " + str(round(self.slider_b.value()/250*100,2)) +  " % - BLUE LED")
 
    def command_mode(self):
        if(self.serial_port.isOpen() == True):
            for i in "+++":
                self.serial_port.write(i.encode())
                time.sleep(0.001)
            while(self.serial_port.in_waiting < 2):
                pass
            serialString = self.serial_port.readline()
            self.textbox.appendPlainText("Command mode: " + serialString.decode())

    def set_color(self):
        if(self.locker):
            self.locker = 0
            self.btn_color.setText("Set color")
            self.label_blue.setText("PWM: " + str(round(self.slider_b.value()/250*100,2)) +  "% - BLUE LED")
            self.label_green.setText("PWM: " + str(round(self.slider_g.value()/250*100,2)) +  "% - GREEN LED")
            self.label_red.setText("PWM: " + str(round(self.slider_r.value()/250*100,2)) +  "% - RED LED") 
        else:
            self.locker = 1
            self.btn_color.setText("Change color")
            
 
 
 #Main code
app = QApplication([])
app.setStyle("Fusion")
window = DashBoard()
window.show()
app.exec()
